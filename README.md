
# yocto-training

# Windows Instructions (Docker Toolbox)
### Install docker toolbox

Follow instructions  [https://docs.docker.com/toolbox/toolbox_install_windows/](https://docs.docker.com/toolbox/toolbox_install_windows/)
If you have Windows 10 or above install Docker Desktop
[https://docs.docker.com/docker-for-windows/install/](https://docs.docker.com/docker-for-windows/install/)
### Change default vm settings

The default Virtual Box VM does not provide enough resources to give a good experience when building Yocto images. We recommend you create a new VM with at least 2 CPUs and 4 GB of memory.

-   Run  **Docker Quickstart Terminal**  and then run the following commands in that terminal.
    
-   Remove the default vm
    
```
    docker-machine rm default
```
    
-   Re-create the default vm
    
    -   Choose the number of cpus with  **--virtualbox-cpu-count**. For this example we'll use two.
    -   Choose the amount of RAM:  **--virtualbox-memory**. This is also based on the host hardware. However, choose at least 4GB.
    -   Choose the amount of disk space:  **--virtualbox-disk-size**. It is recommended that this be at least 100 GB since building generates a lot* of output. In this example we'll choose 150GB.
    -   Create vm with new settings
    
```
    docker-machine create -d virtualbox --virtualbox-cpu-count=2 --virtualbox-memory=4096 --virtualbox-disk-size=153600 default
```
    
-   Restart docker
    
```
    docker-machine stop
    exit
```
    

Then start open a new Docker Quickstart Terminal.

### Create the samba container

-   In the new quickstart terminal create a volume as follows.
    
```
    docker volume create --name myvolume
    docker run -it --rm -v myvolume:/workdir busybox chown -R 1000:1000 /workdir
```
    
-   Create a samba container that will allow you to see the files in the volume.
    
```
    docker create -t -p 445:445 --name samba -v myvolume:/workdir crops/samba
```
    
-   Start the samba container
    
```
    docker start samba 
```
    
-   Get the ip address used to talk to samba and open /workdir
    
```
    docker-machine ip
```
    
-   The result of this command is the address you will use to see the workdir. In this example let's say the command returned 192.168.99.100. Now, to see the  _workdir_  open the file browser in windows (win+e) and type
    
```
    \\192.168.99.100\workdir
```
    

## Using the poky container

Before using the poky container, make sure the samba container is running. Note that if you have started it in a previous terminal it will still be running. Run poky container as follows, note that we use the volume created above when specifying the workdir.

```
docker run -it -v myvolume:/workdir crops/poky --workdir=/workdir
```

If you have existing poky container you can use rm switch to remove existing one. 

    docker run --rm -it -v myvolume:/workdir crops/poky --workdir=/workdir

You will see a prompt that looks like

`pokyuser@892e5d2574d6:/workdir$`

Although this is called the poky container, it does not include the bitbake meta-data for the Yocto Project poky distro, instead it is a Linux environment with all dependencies already installed.

For NXP BSP Release you will need additional packages. For this create a new Docker Quickstart Terminal. 
Find the ID of the poky container by running the following command:

    docker container ls
This shows the container ID as follows:

    CONTAINER ID        IMAGE               COMMAND                  CREATED
         STATUS              PORTS                  NAMES
    c885e41c6be3        crops/poky          "/usr/bin/dumb-init "   13 seconds ago
        Up 11 seconds                              eloquent_allen
    6630a8a84e23        crops/samba         "samba.sh"               2 days ago
         Up 26 seconds       0.0.0.0:445->445/tcp   samba
Then execute the following command to install additional packages.

    docker exec -u root c885e41c6be3 apt-get install -y libsdl1.2-dev xterm sed cvs subversion coreutils texi2html \
    docbook-utils python-pysqlite2 help2man make gcc g++ desktop-file-utils \
    libgl1-mesa-dev libglu1-mesa-dev mercurial autoconf automake groff curl lzop asciidoc u-boot-tools
Then create the repo utility in the workdir/bin folder

    $ mkdir bin
    
    $ curl http://commondatastorage.googleapis.com/git-repo-downloads/repo > bin/repo
    
    $ chmod  a+x bin/repo
    
    $ export PATH=/workdir/bin:$PATH
Configure Git settings

    $ git config --global user.name "Your Name"
    
    $ git config --global user.email "Your Email"
    
    $ git config -list
Initialize the repository with latest NXP release and download the Yocto project along with the BSP

    $ mkdir  imx-yocto-bsp
    
    $ cd imx-yocto-bsp
    
    $ repo init -u https://source.codeaurora.org/external/imx/imx-manifest -b imx-linux-sumo -m  imx-4.14.98-2.0.0_ga.xml
    
    $ repo sync
Execute `fsl-setup-release.sh` with the required machine configuration for imx6ulevk

    DISTRO=fsl-imx-fb MACHINE=imx6ulevk source fsl-setup-release.sh -b imx6ulevk_build
Create the deployable image by running

    bitbake fsl-image-validation-imx
Wait for 4-5 hours to compile and generate the image
**fsl-image-validation-imx-imx6ulevk.sdcard.bz2** contains the image that can be installed into an SD card. Extract fsl-image-validation-imx-imx6ulevk.sdcard and install it using Rufus or dd tool.
You can download from [https://rufus.ie/](https://rufus.ie/)