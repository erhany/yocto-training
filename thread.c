/*****************************************************************************
* FILE: pthread.c
* DESCRIPTION:
*   This example program illustrates the use of mutex variables 
*   in a threads program.
******************************************************************************/
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

/*   
The following structure contains the necessary information  
to allow the function "myfunc" to access its input data and 
place its output into the structure.  
*/

/* Define globally accessible variables and a mutex */

#define NUMTHRDS 4
   double sum = 0; 
   pthread_t callThd[NUMTHRDS];
   pthread_mutex_t mutexsum;

/*
The function myfunc is activated when the thread is created.
when a thread is created we pass a single
argument to the activated function - typically this argument
is a thread number. All  the other information required by the 
function is accessed from the globally accessible structure. 
*/

void *myfunc(void *arg)
{

   int threadNum = (int) arg;
/*
Lock a mutex prior to updating the value in the shared
structure, and unlock it upon updating.
*/
   pthread_mutex_lock (&mutexsum);
   sum += threadNum;
   printf("I am thread %d. mysum=%f\n", threadNum, sum);
   pthread_mutex_unlock (&mutexsum);

   pthread_exit((void*) 0);
}

/* 
The main program creates threads which do all the work and then 
print out result upon completion. Before creating the threads,
The input data is created. Since all threads update a shared structure, we
need a mutex for mutual exclusion. The main thread needs to wait for
all threads to complete, it waits for each one of the threads. We specify
a thread attribute value that allow the main thread to join with the
threads it creates. Note also that we free up handles  when they are
no longer needed.
*/

int main (int argc, char *argv[])
{
long i;
double *a, *b;
void *status;
pthread_attr_t attr;

pthread_mutex_init(&mutexsum, NULL);
         
/* Create threads to perform the dotproduct  */
pthread_attr_init(&attr);
pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

for(i=0;i<NUMTHRDS;i++)
  {
   pthread_create(&callThd[i], &attr, myfunc, (void *)i); 
  }

pthread_attr_destroy(&attr);
/* Wait on the other threads */

for(i=0;i<NUMTHRDS;i++) {
  pthread_join(callThd[i], &status);
}

printf ("All threads gone. Sum =  %f \n", sum);
pthread_mutex_destroy(&mutexsum);
pthread_exit(NULL);
}   

